provider "aws" {
  region = "ap-south-1"
}

resource "aws_vpc" "myvpc" {
  cidr_block = "192.168.4.0/24"
  tags = {
     Name = "vpc01"
  }
}

resource "aws_subnet" "mysubnet" {
  vpc_id = "aws_vpc.myvpc.id"
  cidr_block = "192.168.4.0/25"
  tags = {
     Name = "subnet"
   }
}


resource "aws_security_group" "mysq" {
      tags = {
   	Name = "mysq"
   }  
   ingress {
      protocol = "tcp"
      from_port = 0
      to_port = 0
      cidr_blocks = ["0.0.0.0/0"]
   }
   egress {
      protocol = "tcp"
      from_port = 0
      to_port = 0
      cidr_blocks = ["0.0.0.0/0"]
   }
}

resource "aws_instance" "terraform" {
ami = "ami-02e60be79e78fef21"
instance_type = "t2.micro"
subnet_id = "aws_subnet.mysubnet.id"
iam_instance_profile = "ssmrole"
user_data = <<-EOT
             #!/bin/bash
             sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
             sudo systemctl start amazon-ssm-agent
EOT

tags = {
  Name = "terraform"
}
}

terraform {
  backend "s3" {
    bucket = "lathabucket"
    key    = "terraform.tfstate"
    region = "ap-south-1"

  }
}